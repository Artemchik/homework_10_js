const elements = document.querySelectorAll(".tabs-title");
elements.forEach(elem => elem.onclick = onClick);


//-------//
function contentShower(arg){
         if (arg.classList.contains("title_akali")){                                                                                              //AKALI
             document.querySelector(".akali_text").classList.add("tab_active");
             if (arg.classList.contains("active") === false){
                 document.querySelector(".akali_text").classList.remove("tab_active");
             }
         }else if (arg.classList.contains("title_anvia")){                                                                                    //ANVIA
             document.querySelector(".anvia_text").classList.add("tab_active");
             if (arg.classList.contains("active") === false){
                 document.querySelector(".anvia_text").classList.remove("tab_active");
             }
         }else if (arg.classList.contains("title_draven")){                                                                                 //DRAVEN
             document.querySelector(".draven_text").classList.add("tab_active");
             if (arg.classList.contains("active") === false){
                 document.querySelector(".draven_text").classList.remove("tab_active");
             }
         }else if (arg.classList.contains("title_garen")){                                                                                   //GAREN
             document.querySelector(".garen_text").classList.add("tab_active");
             if (arg.classList.contains("active") === false){
                 document.querySelector(".garen_text").classList.remove("tab_active");
             }
         }else if (arg.classList.contains("title_katarina")){                                                                               //KATARINA
             document.querySelector(".katarina_text").classList.add("tab_active");
             if (arg.classList.contains("active") === false){
                 document.querySelector(".katarina_text").classList.remove("tab_active");
             }
         }else{
             console.log("Что-то пошло не так");
         }
}
function onClick(){
    if (this.classList.contains("active")){
        this.classList.remove("active");
        contentShower(this);
    }else{
        this.classList.add("active");
        contentShower(this);
    }
}
